from sklearn.ensemble import RandomForestRegressor
from sklearn.tree import DecisionTreeRegressor
from sklearn.neighbors import KNeighborsRegressor
from sklearn.model_selection import train_test_split
from sklearn.metrics import mean_squared_error as mse


import math

class Regression():

    def __init__(self):
        self.rfr = RandomForestRegressor()
        self.dtr = DecisionTreeRegressor()
        self.knr = KNeighborsRegressor()

    def eval_model(self, x, y):
        x_train, x_test, y_train, y_test = train_test_split(x, y, test_size=0.3, random_state=42)

        self.rfr.fit(x_train, y_train)
        predict_train = self.rfr.predict(x_train)
        predict_test  = self.rfr.predict(x_test)

        print("Evaluation Results for RandomForestRegressor: ")
        print('RMSE on training data: ', math.sqrt(mse(y_train, predict_train)))
        print('RMSE on test data: ',  math.sqrt(mse(y_test, predict_test)))

        self.dtr.fit(x_train, y_train)
        predict_train = self.dtr.predict(x_train)
        predict_test  = self.dtr.predict(x_test)

        print("Evaluation Results for DecisionTreeRegressor: ")
        print('RMSE on training data: ', math.sqrt(mse(y_train, predict_train)))
        print('RMSE on test data: ',  math.sqrt(mse(y_test, predict_test)))

        self.knr.fit(x_train, y_train)

        predict_train = self.knr.predict(x_train)
        predict_test  = self.knr.predict(x_test)

        print("Evaluation Results for KNeighborsRegressor: ")
        print('RMSE on training data: ', math.sqrt(mse(y_train, predict_train)))
        print('RMSE on test data: ',  math.sqrt(mse(y_test, predict_test)))
