from sklearn.ensemble import RandomForestClassifier
from sklearn.tree import DecisionTreeClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score

class Classification():

    def __init__(self):
        self.rfc = RandomForestClassifier()
        self.dtc = DecisionTreeClassifier()
        self.knc = KNeighborsClassifier()

    def eval_model(self, x, y):

        x_train, x_test, y_train, y_test = train_test_split(x, y, test_size=0.25, random_state=42)

        self.rfc.fit(x_train, y_train)

        predict_train = self.rfc.predict(x_train)
        predict_test  = self.rfc.predict(x_test)

        print("Evaluation Results for RandomForestClassifier: ")
        print('Accuracy score on training data: ', accuracy_score(y_train, predict_train))
        print('Accuracy score on test data: ',  accuracy_score(y_test, predict_test))

        self.dtc.fit(x_train, y_train)

        predict_train = self.dtc.predict(x_train)
        predict_test  = self.dtc.predict(x_test)

        print("Evaluation Results for DecisionTreeClassifier: ")
        print('Accuracy score on training data: ', accuracy_score(y_train, predict_train))
        print('Accuracy score on test data: ',  accuracy_score(y_test, predict_test))

        self.knc.fit(x_train, y_train)

        predict_train = self.knc.predict(x_train)
        predict_test  = self.knc.predict(x_test)

        print("Evaluation Results for KNeighborsClassifier: ")
        print('Accuracy score on training data: ', accuracy_score(y_train, predict_train))
        print('Accuracy score on test data: ',  accuracy_score(y_test, predict_test))
