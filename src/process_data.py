import sqlite3
import pandas
import numpy as np

class Process_Data():

    def __init__():
        pass

    def import_from_db():
        con = sqlite3.connect('data/home_sales.db')
        df = pandas.read_sql_query('SELECT * FROM sales', con)
        df['condition'] = df['condition'].str.lower()
        df.replace(to_replace='terrible', value='1.0', inplace=True)
        df.replace(to_replace='poor', value='2.0', inplace=True)
        df.replace(to_replace='fair', value='3.0', inplace=True)
        df.replace(to_replace='good', value='4.0', inplace=True)
        df.replace(to_replace='excellent', value='5.0', inplace=True)
        df = df.astype({'condition': 'float64'})

        # simple reordering to have target column as the last column
        cols = list(df.columns.values)
        cols.remove('price')
        cols.append('price')
        df = df[cols]
        df = df.drop(columns=['id', 'date', 'floors', 'condition', 'review_score', 'renovation'])
        df = df[df['price'].notna()]

        labels = ["{0} - {1}".format(i, i + 99999) for i in range(0, 7900000, 100000)]
        df['price_group'] = pandas.cut(df['price'], range(0, 8000000, 100000), right=False, labels=labels)
        print(df.price_group.sort_values().unique())

        print(df)
        return df
