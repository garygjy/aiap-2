import pandas

from sklearn import pipeline
from sklearn.impute import KNNImputer

from process_data import Process_Data
from regress_ai import Regression
from classify_ai import Classification

class myPipeline():

    def __init__(self):
        self.import_data()
        self.impute_data()
        pass

    def import_data(self):
        print("Fetching data from database")
        self.data = Process_Data.import_from_db()
        self.x = self.data.drop(columns=['price_group', 'price'])
        self.y_num = self.data['price']
        self.y_cat = self.data['price_group']

    def impute_data(self):
        print("Imputing missing data")
        imputer = KNNImputer(n_neighbors=20, weights='distance')
        imputer.fit(self.x, self.y_num)
        x_trans = imputer.transform(self.x)
        column_names = ['bedrooms', 'bathrooms', 'waterfront', 'view',
                        'basement_size', 'built','zipcode', 'latitude',
                        'longtitude', 'living_room_size', 'lot_size']
        self.x = pandas.DataFrame.from_records(x_trans, columns=column_names)


    def main(self):
        print("Ready to evaluate")
        while True:
            print("Please choose the approach")
            print("Enter 1 for regression, 2 for classification, 3 to end")
            print("press Ctrl-D at anytime to exit")
            temp = input()
            if len(temp.split()) >= 2:
                print("Too many inputs given. Please try again")
                continue
            if temp == '1':
                print("Selected Regression Approach")
                regress = Regression()
                regress.eval_model(self.x, self.y_num)

            elif temp == '2':
                print("Selected Classification Approach")
                classify = Classification()
                classify.eval_model(self.x, self.y_cat)

            elif temp == '3':
                break
            else:
                print("Invalid input. Please try again")


if __name__ == '__main__':
    ml_pipeline = myPipeline()
    ml_pipeline.main()
