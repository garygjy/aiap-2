# Machine Learning Pipeline

### Pipeline Breakdown

- Step 1: Fetching and processing of data _( module_name: process_data.py )_

  - The first step of the pipeline is to fetch the data from the url and do some preprocessing to ensure there are no missing values in the data obtained
  - Preprocessing was done to replace negative numbers with zero, fix spelling errors, difference in casing for the values under


- Step 2: Imputation of data _( module_name:  )_

  - The second step of the pipeline is the imputation of data, where missing values are infered from other values. The KNNImputer is used to better infer values from neighboring data points as the pricing of houses are similar for houses of similar design.

- Step 3: Evaluation of models

  - The user can now enter values to choose which sets of models to evaluate. More information can be found in the `Executing the Pipeline` Section

### Executing the pipeline
1. Upon running _'run.sh'_, the pipeline will initialize and begin retrieving the   dataset from the .db file. Following that, preprocessing work will begin to take care of invalid values such as missing values for the _price_ column. Imputation of data will begin to replace NaN / null values in the features columns.

2. Once the fetching of dataset and preprocessing of data is complete, user may enter 1, 2 or 3 to proceed to the next step

| Input 	|  Desc.                                                               	|
|-------	|---------------------------------------------------------------------	|
| 1     	| Selects the regression models for evaluation                          |
| 2     	| Selects the classification models for evaluation                    	|
| 3    	  | Stops the program                                                      |


  Once inputs are entered, the program will proceed to evaluate the selected set of models and print the values of relevant metrics used


### Choice of Models

Regressors used: RandomForestRegressor, DecisionTreeRegressor and KNeighborsRegressor

Classifiers used: RandomForestClassifier, DecisionTreeClassifier and KNeighborsClassifier

Based on the flowchart provided at https://scikit-learn.org/stable/tutorial/machine_learning_map/index.html, the dataset consists of 21896 rows of data. It has been determined through exploratory data analysis that 11 features will be used to fit the model and therefore, following the path in the flowchart, we arrive at ensemble regressors for regression models, and KNeighbors classifier and ensemble classifiers if it is not working.

### Evaluation of Regressor Models
Making use of the mean_squared_error metric in the _scikit-learn_ library, the Root Mean Squared Error (RMSE) of the regression models are, rounded to 2 decimal places, as follows:

| Model 	| RMSE (train)     	| RMSE (test)                                                               	|
|-------	|-------------	|---------------------------------------------------------------------	|
| RFR   	| 59132.78        	|  177743.76                                                	|
| DTR     	| 8320.56    	| 252450.39                                                              |
| KNR  	  | 227711.94 	| 305293.94                                                      	|

Based on the metrics above, the best model is the RandomForestRegressor, which has the smallest RMSE for the test set. Even though a smaller RMSE for the training set is observed with the DecisionTreeRegressor, the larger RMSE for the test set cannot be ignored. The KNeighborsRegressor registered the largest RMSE values across both training and test set among the 3 models.

### Evaluation of Classifier Models
Making use of the accuracy_score metric in the _scikit-learn_ library, the accuracy score of the classification models are, rounded to 2 decimal places, as follows:

| Model 	| Accuracy score (train)     	| Accuracy score (test)                                                               	|
|-------	|-------------	|---------------------------------------------------------------------	|
| RFC   	| 0.99586        	|  0.50840                                                	|
| DTC     	| 0.99586    	| 0.43807                                                              |
| KNC  	  | 0.48898 	| 0.25959                                                      	|

Based on the metrics above, the best model is the RandomForestClassifier, with the accuracy score closest to 1 for both training and test set.

### Final Evaluation

For pricing of houses, it may be better to use classifiers as the buyers will bargain for lower prices and if the real estate agents can have a range for the price to work with, deals may be made easier.




### Folder Structure and Outline

The folder structure is as follows:
.
├── data
│   └── home_sales.db
├── eda.ipynb
├── README.md
├── requirements.txt
├── run.sh
└── src
    ├── classify_ai.py
    ├── pipeline.py
    ├── process_data.py
    │   ├── classify_ai.cpython-36.pyc
    │   ├── process_data.cpython-36.pyc
    │   └── regress_ai.cpython-36.pyc
    └── regress_ai.py


### Personal information

Name: Gary Goh Jiang Yu
Email: ggjy03@gmail.com
